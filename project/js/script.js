
1// DOM - це засіб для роботи зі структурою документа, а також з елементами сторінки в кодах HTML та СSS
2// innerText повертає nskmrb текстовий вміст в елементі, innerHTML показує все що написано разом з відкриваючими і закриваючими тегами
3// getElementById, getElementsByName, getElementsByTagName, getElementsByClassName, querySelectorAll, querySelector 
// найчастіше використовується - querySelectorAll


1//
const pElement = document.getElementsByTagName("p");
for(let elem of pElement){
elem.style = "background-color: #ff0000"
}

2//
console.log(document.getElementById("optionsList"));

console.log(document.getElementById("optionsList").parentElement);

console.log(document.getElementById("optionsList").childNodes);


const childType = document.getElementById("optionsList").childNodes;
for(let elem of childType){
    console.log(elem.nodeType)
}

const nameNode = document.getElementById("optionsList").childNodes;
for(let elem of nameNode){
    console.log(elem.nodeName)
}

3//

document.getElementById("testParagraph").textContent = "This is a paragraph";

4//

const newItem = document.querySelector(".main-header").children
console.log(newItem)
for (let i = 0; i < newItem.length; i++) {
   newItem[i].classList.add("nav-item");
}
console.log(newItem)

5//
const allItem = document.querySelectorAll(".section-title")
allItem.forEach((elem) => {
    elem.classList.remove(".section-title")
})
