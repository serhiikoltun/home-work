

function createList(array, parent = document.body) {
  
    let listElement = document.createElement("ul");
    let listItem = document.createElement("li");
  
    
    parent.appendChild(listElement);
  
    for (let i = 0; i < array.length; ++i) {
      listItem.textContent = array[i];
      listElement.appendChild(listItem);
      listItem = document.createElement("li");
    }
  }
  
  createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
  
  