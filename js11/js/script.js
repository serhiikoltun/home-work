


const togglePassword = document.querySelector("#togglePassword");
const passwordInput = document.querySelector("#password");

togglePassword.addEventListener("click", function () {
  const type = passwordInput.getAttribute("type") === "password" ? "text" : "password";
  passwordInput.setAttribute("type", type);
  togglePassword.classList.toggle("fa-eye");
  togglePassword.classList.toggle("fa-eye-slash");
});

const toggleConfirmPassword = document.querySelector("#toggleConfirmPassword");
const confirmPasswordInput = document.querySelector("#confirmPassword");

toggleConfirmPassword.addEventListener("click", function () {
  const type =
    confirmPasswordInput.getAttribute("type") === "password"
      ? "text"
      : "password";
  confirmPasswordInput.setAttribute("type", type);
  toggleConfirmPassword.classList.toggle("fa-eye-slash");
  toggleConfirmPassword.classList.toggle("fa-eye");
});


const errorText = document.createElement('span');
errorText.classList.add('error-text');
errorText.innerText = 'Потрібно ввести однакові значення';
confirmPasswordInput.insertAdjacentElement('afterend', errorText);


document.querySelector('form').addEventListener('submit', (e) => {
  e.preventDefault();
  if (passwordInput.value !== confirmPasswordInput.value) {
    errorText.style.display = 'block';
    errorText.style.color = 'red';
    confirmPasswordInput.style.border = '1px solid red';
  } else {
    alert('You are welcome!');
  }
});
