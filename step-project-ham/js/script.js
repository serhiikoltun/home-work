

let tab = document.querySelectorAll('.tabs-title');
tab.forEach(function(item){
    item.addEventListener('click', function(){
        let currentTab = document.querySelector(`.tab-content[data-tab-content="${item.dataset.tab}"]`);

        document.querySelector('.tab-content.content-active').classList.remove('content-active');
        document.querySelector('.tabs-title.active').classList.remove('active');

        currentTab.classList.add('content-active');
        item.classList.add('active');
    });
});




const imagesPerLoad = 4;
let currentFolder = 'all';
let currentImageIndex = imagesPerLoad;

const images = {
  all: [
    '../step-project-ham/img/graphic design/graphic-design1.jpg',
    '../step-project-ham/img/graphic design/graphic-design2.jpg',
    '../step-project-ham/img/graphic design/graphic-design3.jpg',
    '../step-project-ham/img/graphic design/graphic-design4.jpg',
    '../step-project-ham/img/graphic design/graphic-design5.jpg',
    '../step-project-ham/img/graphic design/graphic-design6.jpg',
    '../step-project-ham/img/graphic design/graphic-design7.jpg',
    '../step-project-ham/img/graphic design/graphic-design8.jpg',
    '../step-project-ham/img/graphic design/graphic-design9.jpg',
    '../step-project-ham/img/graphic design/graphic-design10.jpg',
    '../step-project-ham/img/graphic design/graphic-design11.jpg',
    '../step-project-ham/img/graphic design/graphic-design12.jpg',
     '../step-project-ham/img/landing page/landing-page1.jpg',
    '../step-project-ham/img/landing page/landing-page2.jpg', 
    '../step-project-ham/img/landing page/landing-page3.jpg',
    '../step-project-ham/img/landing page/landing-page4.jpg',
    '../step-project-ham/img/landing page/landing-page5.jpg',
    '../step-project-ham/img/landing page/landing-page6.jpg',
    '../step-project-ham/img/landing page/landing-page7.jpg',
    '../step-project-ham/img/web design/web-design1.jpg',
    '../step-project-ham/img/web design/web-design2.jpg',
    '../step-project-ham/img/web design/web-design3.jpg',
    '../step-project-ham/img/web design/web-design4.jpg',
    '../step-project-ham/img/web design/web-design5.jpg',
    '../step-project-ham/img/web design/web-design6.jpg',
    '../step-project-ham/img/web design/web-design7.jpg',
    '../step-project-ham/img/wordpress/wordpress1.jpg',
    '../step-project-ham/img/wordpress/wordpress2.jpg',
    '../step-project-ham/img/wordpress/wordpress3.jpg',
    '../step-project-ham/img/wordpress/wordpress4.jpg',
    '../step-project-ham/img/wordpress/wordpress5.jpg',
    '../step-project-ham/img/wordpress/wordpress6.jpg',
    '../step-project-ham/img/wordpress/wordpress7.jpg',
    '../step-project-ham/img/wordpress/wordpress8.jpg',
    '../step-project-ham/img/wordpress/wordpress9.jpg',
    '../step-project-ham/img/wordpress/wordpress10.jpg',
  ],
  folder1: [
    '../step-project-ham/img/graphic design/graphic-design1.jpg',
    '../step-project-ham/img/graphic design/graphic-design2.jpg',
    '../step-project-ham/img/graphic design/graphic-design3.jpg',
    '../step-project-ham/img/graphic design/graphic-design4.jpg',
    '../step-project-ham/img/graphic design/graphic-design5.jpg',
    '../step-project-ham/img/graphic design/graphic-design6.jpg',
    '../step-project-ham/img/graphic design/graphic-design7.jpg',
    '../step-project-ham/img/graphic design/graphic-design8.jpg',
    '../step-project-ham/img/graphic design/graphic-design9.jpg',
    '../step-project-ham/img/graphic design/graphic-design10.jpg',
    '../step-project-ham/img/graphic design/graphic-design11.jpg',
    '../step-project-ham/img/graphic design/graphic-design12.jpg',
   
    
  ],
  folder2: [
    '../step-project-ham/img/landing page/landing-page1.jpg',
    '../step-project-ham/img/landing page/landing-page2.jpg', 
    '../step-project-ham/img/landing page/landing-page3.jpg',
    '../step-project-ham/img/landing page/landing-page4.jpg',
    '../step-project-ham/img/landing page/landing-page5.jpg',
    '../step-project-ham/img/landing page/landing-page6.jpg',
    '../step-project-ham/img/landing page/landing-page7.jpg',
  ],
  folder3: [
    '../step-project-ham/img/web design/web-design1.jpg',
    '../step-project-ham/img/web design/web-design2.jpg',
    '../step-project-ham/img/web design/web-design3.jpg',
    '../step-project-ham/img/web design/web-design4.jpg',
    '../step-project-ham/img/web design/web-design5.jpg',
    '../step-project-ham/img/web design/web-design6.jpg',
    '../step-project-ham/img/web design/web-design7.jpg',
  ],
  folder4: [
    '../step-project-ham/img/wordpress/wordpress1.jpg',
    '../step-project-ham/img/wordpress/wordpress2.jpg',
    '../step-project-ham/img/wordpress/wordpress3.jpg',
    '../step-project-ham/img/wordpress/wordpress4.jpg',
    '../step-project-ham/img/wordpress/wordpress5.jpg',
    '../step-project-ham/img/wordpress/wordpress6.jpg',
    '../step-project-ham/img/wordpress/wordpress7.jpg',
    '../step-project-ham/img/wordpress/wordpress8.jpg',
    '../step-project-ham/img/wordpress/wordpress9.jpg',
    '../step-project-ham/img/wordpress/wordpress10.jpg',
  ]
};

function showImages(folder) {
    currentFolder = folder;
    currentImageIndex = imagesPerLoad;
    
    const imageContainer = document.getElementById('imageContainer');
    imageContainer.innerHTML = '';
    
    let folderImages = [];
    
    if (folder === 'all') {
      for (const folderKey in images) {
        if (folderKey == 'all') {
          folderImages = folderImages.concat(images[folderKey]);
        }
      }
    } else {
      folderImages = images[folder];
    }
    
    const totalImages = Math.min(currentImageIndex, folderImages.length);
    
    for (let i = 0; i < totalImages; i++) {
      const image = document.createElement('img');
      image.src = folderImages[i];
      imageContainer.appendChild(image);
    }
    
    const loadMoreButton = document.getElementById('loadMoreButton');
    loadMoreButton.style.display = 'block';
    if (currentImageIndex >= folderImages.length) {
      loadMoreButton.style.display = 'none';
    }
    
    const buttons = document.querySelectorAll('.buttoms buttom');
    buttons.forEach(button => {
      button.classList.remove('active');
      if (button.textContent.toLowerCase() === folder.toLowerCase()) {
        button.classList.add('active');
      }
    });
  }
  
  function loadMoreImages() {
    const folderImages = images[currentFolder];
    
    const imageContainer = document.getElementById('imageContainer');
    
    let additionalImages = [];
    
    if (currentFolder === 'all') {
      for (const folderKey in images) {
        if (folderKey !== 'all') {
          additionalImages = additionalImages.concat(images[folderKey]);
        }
      }
    }
    
    const startIndex = currentImageIndex;
    const endIndex = currentImageIndex + imagesPerLoad;
    
    for (let i = startIndex; i < endIndex; i++) {
      if (currentFolder === 'all' && i < folderImages.length) {
        const image = document.createElement('img');
        image.src = folderImages[i];
        imageContainer.appendChild(image);
      } else if (i < startIndex + additionalImages.length) {
        const image = document.createElement('img');
        image.src = additionalImages[i - startIndex];
        imageContainer.appendChild(image);
      }
    }
    
    currentImageIndex += imagesPerLoad;
    
    const loadMoreButton = document.getElementById('loadMoreButton');
    if (currentImageIndex >= folderImages.length + additionalImages.length && !hasMoreImages()) {
      loadMoreButton.style.display = 'none';
    }
  }
  
  function hasMoreImages() {
    const folderImages = images[currentFolder];
    let additionalImages = [];
    
    if (currentFolder === 'all') {
      for (const folderKey in images) {
        if (folderKey !== 'all') {
          additionalImages = additionalImages.concat(images[folderKey]);
        }
      }
    }
    
    return currentImageIndex < folderImages.length + additionalImages.length;
  }
  
  showImages('all');
  






  document.addEventListener("DOMContentLoaded", function() {
    let carouselItems = document.querySelectorAll(".carousel-item");
    let carouselThumbnails = document.querySelectorAll(".carousel-thumbnails img");
    let carouselControls = document.querySelectorAll(".carousel-control-prev, .carousel-control-next");
  
    let currentIndex = 0;
  
    function showItem(index) {
      if (index < 0 || index >= carouselItems.length) {
        return;
      }
  
      carouselItems[currentIndex].classList.remove("active");
      carouselItems[index].classList.add("active");
  
      carouselThumbnails[currentIndex].classList.remove("active");
      carouselThumbnails[index].classList.add("active");
  
      currentIndex = index;
    }
  
    function nextItem() {
      let newIndex = currentIndex + 1;
      if (newIndex >= carouselItems.length) {
        newIndex = 0;
      }
      showItem(newIndex);
    }
  
    function prevItem() {
      let newIndex = currentIndex - 1;
      if (newIndex < 0) {
        newIndex = carouselItems.length - 1;
      }
      showItem(newIndex);
    }
  
    function selectThumbnail() {
      let newIndex = parseInt(this.getAttribute("data-index"));
      showItem(newIndex);
    }
  
    carouselControls[0].addEventListener("click", prevItem);
    carouselControls[1].addEventListener("click", nextItem);
  
    for (let i = 0; i < carouselThumbnails.length; i++) {
      carouselThumbnails[i].addEventListener("click", selectThumbnail);
    }
  
    showItem(0);
  });
    
































