// 1. екранування існує для того щоб ми могли вписати будь-який спеціальний символ в код за допомогою зворотнього слешу
// 2. фанкшен декларейшен за допогою ключового слова фанкшен і назви
//    фанкшен експресіон функція немає ім'я, але записується в змінну
//    стрілкова за допомогою стрілки =>
// 3. це механізм джаваскрипт, в якому змінні і оголошення функцій, пересуваються вгору своєї області видимості перед тим, як код буде виконано.


function createNewUser() {
  const newUser = {
    firstName: prompt("enter first name"),
    lastName: prompt("enter last name"),
    birthday: new Date(prompt("enter data")),
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge(){
      let today = new Date();
      let birthDate = new Date(this.birthday);
      let age = today.getFullYear() - birthDate.getFullYear();
      let month = today.getMonth() - birthDate.getMonth();
      if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    },
    getPassword(){
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    }
  };
  return newUser;
}
const user = createNewUser()
console.log(user.getLogin());
console.log(user.getAge())
console.log(user.getPassword())



