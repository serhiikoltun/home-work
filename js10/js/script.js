

const tabs = document.querySelectorAll(".tabs-title");
const tabContents = document.querySelectorAll(".tabs-content li");

tabs.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    tabs.forEach((tab) => tab.classList.remove("active"));
    tabContents.forEach((content) => content.classList.remove("active"));

    tab.classList.add("active");
    tabContents[index].classList.add("active");
  });
});
