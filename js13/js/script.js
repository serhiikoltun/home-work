
let images = ["1.jpg", "2.jpg", "3.JPG", "4.png"];

let slider = document.querySelector(".images-wrapper");
let img = slider.querySelector("img");

let i = 1;
img.src = "./img/" + images[0];

const intervalId = window.setInterval(function () {
  img.src = "./img/" + images[i];
  i++;

  if (i == images.length) {
    i = 0;
  }
}, 3000);

function stopSlideShow() {
  clearInterval(intervalId);
}

function runSlideShow() {
  const intervalId = window.setInterval(function () {
    img.src = "./img/" + images[i];
    i++;

    if (i == images.length) {
      i = 0;
    }
  }, 3000);
}
